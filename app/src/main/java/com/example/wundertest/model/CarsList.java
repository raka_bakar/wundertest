package com.example.wundertest.model;


import com.google.gson.annotations.SerializedName;


public class CarsList{

	@SerializedName("zipCode")
	private String zipCode;

	@SerializedName("vehicleTypeId")
	private int vehicleTypeId;

	@SerializedName("fuelLevel")
	private int fuelLevel;

	@SerializedName("address")
	private String address;

	@SerializedName("reservationState")
	private int reservationState;

	@SerializedName("distance")
	private String distance;

	@SerializedName("vehicleStateId")
	private int vehicleStateId;

	@SerializedName("city")
	private String city;

	@SerializedName("licencePlate")
	private String licencePlate;

	@SerializedName("lon")
	private double lon;

	@SerializedName("title")
	private String title;

	@SerializedName("pricingTime")
	private String pricingTime;

	@SerializedName("carId")
	private int carId;

	@SerializedName("isClean")
	private boolean isClean;

	@SerializedName("pricingParking")
	private String pricingParking;

	@SerializedName("locationId")
	private int locationId;

	@SerializedName("lat")
	private double lat;

	@SerializedName("isDamaged")
	private boolean isDamaged;

	public void setZipCode(String zipCode){
		this.zipCode = zipCode;
	}

	public String getZipCode(){
		return zipCode;
	}

	public void setVehicleTypeId(int vehicleTypeId){
		this.vehicleTypeId = vehicleTypeId;
	}

	public int getVehicleTypeId(){
		return vehicleTypeId;
	}

	public void setFuelLevel(int fuelLevel){
		this.fuelLevel = fuelLevel;
	}

	public int getFuelLevel(){
		return fuelLevel;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setReservationState(int reservationState){
		this.reservationState = reservationState;
	}

	public int getReservationState(){
		return reservationState;
	}

	public void setDistance(String distance){
		this.distance = distance;
	}

	public String getDistance(){
		return distance;
	}

	public void setVehicleStateId(int vehicleStateId){
		this.vehicleStateId = vehicleStateId;
	}

	public int getVehicleStateId(){
		return vehicleStateId;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setLicencePlate(String licencePlate){
		this.licencePlate = licencePlate;
	}

	public String getLicencePlate(){
		return licencePlate;
	}

	public void setLon(double lon){
		this.lon = lon;
	}

	public double getLon(){
		return lon;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setPricingTime(String pricingTime){
		this.pricingTime = pricingTime;
	}

	public String getPricingTime(){
		return pricingTime;
	}

	public void setCarId(int carId){
		this.carId = carId;
	}

	public int getCarId(){
		return carId;
	}

	public void setIsClean(boolean isClean){
		this.isClean = isClean;
	}

	public boolean isIsClean(){
		return isClean;
	}

	public void setPricingParking(String pricingParking){
		this.pricingParking = pricingParking;
	}

	public String getPricingParking(){
		return pricingParking;
	}

	public void setLocationId(int locationId){
		this.locationId = locationId;
	}

	public int getLocationId(){
		return locationId;
	}

	public void setLat(double lat){
		this.lat = lat;
	}

	public double getLat(){
		return lat;
	}

	public void setIsDamaged(boolean isDamaged){
		this.isDamaged = isDamaged;
	}

	public boolean isIsDamaged(){
		return isDamaged;
	}

	@Override
 	public String toString(){
		return 
			"CarsList{" + 
			"zipCode = '" + zipCode + '\'' + 
			",vehicleTypeId = '" + vehicleTypeId + '\'' + 
			",fuelLevel = '" + fuelLevel + '\'' + 
			",address = '" + address + '\'' + 
			",reservationState = '" + reservationState + '\'' + 
			",distance = '" + distance + '\'' + 
			",vehicleStateId = '" + vehicleStateId + '\'' + 
			",city = '" + city + '\'' + 
			",licencePlate = '" + licencePlate + '\'' + 
			",lon = '" + lon + '\'' + 
			",title = '" + title + '\'' + 
			",pricingTime = '" + pricingTime + '\'' + 
			",carId = '" + carId + '\'' + 
			",isClean = '" + isClean + '\'' + 
			",pricingParking = '" + pricingParking + '\'' + 
			",locationId = '" + locationId + '\'' + 
			",lat = '" + lat + '\'' + 
			",isDamaged = '" + isDamaged + '\'' + 
			"}";
		}
}