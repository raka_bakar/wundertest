package com.example.wundertest.presenter;

public interface BasePresenter {
    void onDestroy();
}
