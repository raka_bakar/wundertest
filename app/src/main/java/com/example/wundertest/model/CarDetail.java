package com.example.wundertest.model;


import com.google.gson.annotations.SerializedName;


public class CarDetail{

	@SerializedName("zipCode")
	private String zipCode;

	@SerializedName("vehicleTypeId")
	private int vehicleTypeId;

	@SerializedName("fuelLevel")
	private int fuelLevel;

	@SerializedName("address")
	private String address;

	@SerializedName("reservationState")
	private int reservationState;

	@SerializedName("vehicleStateId")
	private int vehicleStateId;

	@SerializedName("city")
	private String city;

	@SerializedName("licencePlate")
	private String licencePlate;

	@SerializedName("lon")
	private double lon;

	@SerializedName("title")
	private String title;

	@SerializedName("pricingTime")
	private String pricingTime;

	@SerializedName("carId")
	private int carId;

	@SerializedName("damageDescription")
	private String damageDescription;

	@SerializedName("isClean")
	private boolean isClean;

	@SerializedName("vehicleTypeImageUrl")
	private String vehicleTypeImageUrl;

	@SerializedName("pricingParking")
	private String pricingParking;

	@SerializedName("hardwareId")
	private String hardwareId;

	@SerializedName("locationId")
	private int locationId;

	@SerializedName("isActivatedByHardware")
	private boolean isActivatedByHardware;

	@SerializedName("lat")
	private double lat;

	@SerializedName("isDamaged")
	private boolean isDamaged;

	public void setZipCode(String zipCode){
		this.zipCode = zipCode;
	}

	public String getZipCode(){
		return zipCode;
	}

	public void setVehicleTypeId(int vehicleTypeId){
		this.vehicleTypeId = vehicleTypeId;
	}

	public int getVehicleTypeId(){
		return vehicleTypeId;
	}

	public void setFuelLevel(int fuelLevel){
		this.fuelLevel = fuelLevel;
	}

	public int getFuelLevel(){
		return fuelLevel;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setReservationState(int reservationState){
		this.reservationState = reservationState;
	}

	public int getReservationState(){
		return reservationState;
	}

	public void setVehicleStateId(int vehicleStateId){
		this.vehicleStateId = vehicleStateId;
	}

	public int getVehicleStateId(){
		return vehicleStateId;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setLicencePlate(String licencePlate){
		this.licencePlate = licencePlate;
	}

	public String getLicencePlate(){
		return licencePlate;
	}

	public void setLon(double lon){
		this.lon = lon;
	}

	public double getLon(){
		return lon;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setPricingTime(String pricingTime){
		this.pricingTime = pricingTime;
	}

	public String getPricingTime(){
		return pricingTime;
	}

	public void setCarId(int carId){
		this.carId = carId;
	}

	public int getCarId(){
		return carId;
	}

	public void setDamageDescription(String damageDescription){
		this.damageDescription = damageDescription;
	}

	public String getDamageDescription(){
		return damageDescription;
	}

	public void setIsClean(boolean isClean){
		this.isClean = isClean;
	}

	public boolean isIsClean(){
		return isClean;
	}

	public void setVehicleTypeImageUrl(String vehicleTypeImageUrl){
		this.vehicleTypeImageUrl = vehicleTypeImageUrl;
	}

	public String getVehicleTypeImageUrl(){
		return vehicleTypeImageUrl;
	}

	public void setPricingParking(String pricingParking){
		this.pricingParking = pricingParking;
	}

	public String getPricingParking(){
		return pricingParking;
	}

	public void setHardwareId(String hardwareId){
		this.hardwareId = hardwareId;
	}

	public String getHardwareId(){
		return hardwareId;
	}

	public void setLocationId(int locationId){
		this.locationId = locationId;
	}

	public int getLocationId(){
		return locationId;
	}

	public void setIsActivatedByHardware(boolean isActivatedByHardware){
		this.isActivatedByHardware = isActivatedByHardware;
	}

	public boolean isIsActivatedByHardware(){
		return isActivatedByHardware;
	}

	public void setLat(double lat){
		this.lat = lat;
	}

	public double getLat(){
		return lat;
	}

	public void setIsDamaged(boolean isDamaged){
		this.isDamaged = isDamaged;
	}

	public boolean isIsDamaged(){
		return isDamaged;
	}

	@Override
 	public String toString(){
		return 
			"CarDetail{" + 
			"zipCode = '" + zipCode + '\'' + 
			",vehicleTypeId = '" + vehicleTypeId + '\'' + 
			",fuelLevel = '" + fuelLevel + '\'' + 
			",address = '" + address + '\'' + 
			",reservationState = '" + reservationState + '\'' + 
			",vehicleStateId = '" + vehicleStateId + '\'' + 
			",city = '" + city + '\'' + 
			",licencePlate = '" + licencePlate + '\'' + 
			",lon = '" + lon + '\'' + 
			",title = '" + title + '\'' + 
			",pricingTime = '" + pricingTime + '\'' + 
			",carId = '" + carId + '\'' + 
			",damageDescription = '" + damageDescription + '\'' + 
			",isClean = '" + isClean + '\'' + 
			",vehicleTypeImageUrl = '" + vehicleTypeImageUrl + '\'' + 
			",pricingParking = '" + pricingParking + '\'' + 
			",hardwareId = '" + hardwareId + '\'' + 
			",locationId = '" + locationId + '\'' + 
			",isActivatedByHardware = '" + isActivatedByHardware + '\'' + 
			",lat = '" + lat + '\'' + 
			",isDamaged = '" + isDamaged + '\'' + 
			"}";
		}
}