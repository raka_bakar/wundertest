package com.example.wundertest.ui.maps;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.example.wundertest.R;
import com.example.wundertest.ui.cardetail.CarDetailActivity;
import com.example.wundertest.presenter.MapsPresenterImpl;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, MapsContract.view, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    private GoogleMap mMap;
    private Location mLastKnownLocation;
    private List<Marker> listMarker = new ArrayList<>();

    private static final String KEY_LOCATION = "location";
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    boolean isClick = false;
     private MapsPresenterImpl presenter;

    private static int CAR_DETAIL = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
           init();
        getLastLocation(savedInstanceState);
        presenter.loadCarsData();
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void init(){
        presenter = new MapsPresenterImpl(this);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        getLocationPermissions();
        getCurrentLocation();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermissions();
            }
        } catch (SecurityException e)  {
                Log.e("SecurityException", "error message:"+e.getMessage());
        }
    }

    private void getLastLocation(Bundle savedInstanceState){
        if(savedInstanceState!=null){
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
        }
    }

    private void getLocationPermissions() {

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void getCurrentLocation() {

        try {
            if (mLocationPermissionGranted) {

                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        mLastKnownLocation = task.getResult();
                        LatLng currectLoc = new LatLng(mLastKnownLocation.getLatitude(),mLastKnownLocation.getLongitude());
                        MarkerOptions markerOptions = new MarkerOptions().position(currectLoc)
                                .title("Current Location")
                                .draggable(false);
                        Marker marker = mMap.addMarker(markerOptions);
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(currectLoc));
                        listMarker.add(marker);
                    } else {
                        mMap.getUiSettings().setMyLocationButtonEnabled(false);

                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("SecurityException","message="+e.getMessage());
        }
    }


    @Override
    public void addMarker(double lat, double lon, String title, int position,int carId) {

        LatLng car = new LatLng(lat, lon);
        title = checkTitle(title,carId);

        MarkerOptions markerOptions = new MarkerOptions().position(car)
                .title(title)
                .draggable(false);
        mMap.setOnMarkerClickListener(this);
        Marker marker = mMap.addMarker(markerOptions);
        marker.setTag(carId);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(car));
        listMarker.add(marker);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (!isClick && !marker.getTitle().equals("Current Location") ) {
            hideMarker(marker);

        } else if (!marker.getTitle().equals("Current Location")){
            startCarDetailActivity(marker.getTag().toString());

            return true;
        }

        return false;
    }

    private String checkTitle(String title, int carId){
        if(title == null || title.equals("")){
            title = "Car "+carId;
        }
        return title;
    }
    private void hideMarker(Marker marker){
        for (int i = 0; i < listMarker.size(); i++) {
            if (!marker.getId().equals(listMarker.get(i).getId())) {
                listMarker.get(i).setVisible(false);
                isClick = true;
            }
        }
    }

    private void showMarker(){

        for (int i = 0; i < listMarker.size(); i++) {

                listMarker.get(i).setVisible(true);
                isClick = false;

        }
    }
    private void startCarDetailActivity(String carId){
        Intent intent = new Intent(this, CarDetailActivity.class);
        intent.putExtra("CAR_ID", carId);
        startActivityForResult(intent,CAR_DETAIL);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAR_DETAIL){
            showMarker();
        }
    }


    @Override
    public void onMapClick(LatLng latLng) {
        showMarker();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        presenter = null;
    }
}
