package com.example.wundertest.model;


import com.google.gson.annotations.SerializedName;


public class CarRent{

	@SerializedName("cost")
	private int cost;

	@SerializedName("reservationId")
	private int reservationId;

	@SerializedName("fuelCardPin")
	private String fuelCardPin;

	@SerializedName("licencePlate")
	private String licencePlate;

	@SerializedName("startAddress")
	private String startAddress;

	@SerializedName("isParkModeEnabled")
	private boolean isParkModeEnabled;

	@SerializedName("drivenDistance")
	private int drivenDistance;

	@SerializedName("startTime")
	private int startTime;

	@SerializedName("endTime")
	private int endTime;

	@SerializedName("userId")
	private int userId;

	@SerializedName("carId")
	private int carId;

	@SerializedName("damageDescription")
	private String damageDescription;

	public void setCost(int cost){
		this.cost = cost;
	}

	public int getCost(){
		return cost;
	}

	public void setReservationId(int reservationId){
		this.reservationId = reservationId;
	}

	public int getReservationId(){
		return reservationId;
	}

	public void setFuelCardPin(String fuelCardPin){
		this.fuelCardPin = fuelCardPin;
	}

	public String getFuelCardPin(){
		return fuelCardPin;
	}

	public void setLicencePlate(String licencePlate){
		this.licencePlate = licencePlate;
	}

	public String getLicencePlate(){
		return licencePlate;
	}

	public void setStartAddress(String startAddress){
		this.startAddress = startAddress;
	}

	public String getStartAddress(){
		return startAddress;
	}

	public void setIsParkModeEnabled(boolean isParkModeEnabled){
		this.isParkModeEnabled = isParkModeEnabled;
	}

	public boolean isIsParkModeEnabled(){
		return isParkModeEnabled;
	}

	public void setDrivenDistance(int drivenDistance){
		this.drivenDistance = drivenDistance;
	}

	public int getDrivenDistance(){
		return drivenDistance;
	}

	public void setStartTime(int startTime){
		this.startTime = startTime;
	}

	public int getStartTime(){
		return startTime;
	}

	public void setEndTime(int endTime){
		this.endTime = endTime;
	}

	public int getEndTime(){
		return endTime;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setCarId(int carId){
		this.carId = carId;
	}

	public int getCarId(){
		return carId;
	}

	public void setDamageDescription(String damageDescription){
		this.damageDescription = damageDescription;
	}

	public String getDamageDescription(){
		return damageDescription;
	}

	@Override
 	public String toString(){
		return 
			"CarRent{" + 
			"cost = '" + cost + '\'' + 
			",reservationId = '" + reservationId + '\'' + 
			",fuelCardPin = '" + fuelCardPin + '\'' + 
			",licencePlate = '" + licencePlate + '\'' + 
			",startAddress = '" + startAddress + '\'' + 
			",isParkModeEnabled = '" + isParkModeEnabled + '\'' + 
			",drivenDistance = '" + drivenDistance + '\'' + 
			",startTime = '" + startTime + '\'' + 
			",endTime = '" + endTime + '\'' + 
			",userId = '" + userId + '\'' + 
			",carId = '" + carId + '\'' + 
			",damageDescription = '" + damageDescription + '\'' + 
			"}";
		}
}