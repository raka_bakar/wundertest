package com.example.wundertest.network;

import com.example.wundertest.model.CarRent;
import com.google.gson.JsonObject;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RentService {


    @POST("wunderfleet-recruiting-mobile-dev-quick-rental")
    Observable<CarRent> rentVehicle(@Body JsonObject carId );



}
