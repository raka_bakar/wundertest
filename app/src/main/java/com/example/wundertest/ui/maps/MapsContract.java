package com.example.wundertest.ui.maps;

import com.example.wundertest.presenter.BasePresenter;

public interface MapsContract {
    interface view{
        void addMarker(double lat, double lon, String title, int position, int carId);
    }

    interface presenter extends BasePresenter {
        void loadCarsData();

    }

}
