package com.example.wundertest.network;

import com.example.wundertest.model.CarDetail;
import com.example.wundertest.model.CarsList;

import java.util.List;

import io.reactivex.Observable;

import retrofit2.http.GET;
import retrofit2.http.Path;


public interface VehicleService {

    @GET("cars.json")
   Observable<List<CarsList>> getCarsList();

    @GET("cars/{carId}")
    Observable<CarDetail> getCarDetail(@Path("carId") String carId);
}
