package com.example.wundertest.presenter;

import android.util.Log;

import com.example.wundertest.model.CarsList;
import com.example.wundertest.network.ApiVehicle;
import com.example.wundertest.network.VehicleService;
import com.example.wundertest.ui.maps.MapsContract;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class MapsPresenterImpl implements MapsContract.presenter {
    private MapsContract.view view;
    private VehicleService apiService;
    public MapsPresenterImpl(MapsContract.view view) {
        this.view = view;
    }

    @Override
    public void loadCarsData() {
        createService();
        getCarsList();

    }

    private void createService(){
        apiService = ApiVehicle.getInstance().create(VehicleService.class);
    }
    private void getCarsList(){
        apiService.getCarsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CarsList>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<CarsList> carsLists) {
                        addCarMarker(carsLists);
                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.e("getCarsList onError",e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }



    private void addCarMarker(List<CarsList> carsLists){
        for(int i = 0; i<carsLists.size();i++){

            view.addMarker(carsLists.get(i).getLat(),carsLists.get(i).getLon(),carsLists.get(i).getTitle(),i,carsLists.get(i).getCarId());

        }
    }

    @Override
    public void onDestroy() {
        view = null;
    }
}
