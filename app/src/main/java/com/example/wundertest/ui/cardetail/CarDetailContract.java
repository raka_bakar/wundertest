package com.example.wundertest.ui.cardetail;

import com.example.wundertest.presenter.BasePresenter;

public interface CarDetailContract  {
    interface view{
        void showProgressDialog();
        void hideProgessDialog();
        void setCarId(String carId);
        void setName(String title);
        void setCleanlinessStatus(String isClean);
        void setDamagedStatus(String isDamage);
        void setLicensesPlate(String licensesPlate);
        void setFuelLevel(String fuelLevel);
        void setVehicleTypeId(String vehicleTypeId);
        void setPricingTime(String pricingTime);
        void setPricingParking(String pricingParking);
        void setActivatedHardware(String isActivatedHardware);
        void setLocationId(String locationId);
        void setAddress(String address);
        void setZipCode(String zipCode);
        void setCity(String city);
        void setReservationState(String reservationState);
        void setStateId(String stateId);
        void setDamageDescription(String damageDescription);
        void showToast(String message);
        void setCarImage(String url);
        void disableBtn();
        void closeActivity();
    }
    interface presenter extends BasePresenter {
        void loadCarDetail(String carId);

        void rentVehicle(String carId);
    }
}
