package com.example.wundertest.presenter;

import android.util.Log;

import com.example.wundertest.ui.cardetail.CarDetailContract;
import com.example.wundertest.model.CarDetail;
import com.example.wundertest.model.CarRent;
import com.example.wundertest.network.ApiRent;
import com.example.wundertest.network.ApiVehicle;
import com.example.wundertest.network.RentService;
import com.example.wundertest.network.VehicleService;
import com.google.gson.JsonObject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CarDetailPresenterImpl implements CarDetailContract.presenter {

    private  CarDetailContract.view view;
    private VehicleService apiService;
    private RentService rentService;
    public CarDetailPresenterImpl(CarDetailContract.view view) {
        this.view = view;
    }

    @Override
    public void loadCarDetail(String carId) {

        view.showProgressDialog();
       createService();
        getCarDetailData(carId);
    }



    @Override
    public void rentVehicle(String carId) {
        view.showProgressDialog();
        createRentService();
        sendRentData(carId);
    }

    private void createRentService(){
        rentService = ApiRent.getInstance().create(RentService.class);
    }

    private void sendRentData(String carId){

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("carId",carId);
        rentService.rentVehicle(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CarRent>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CarRent carRent) {
                        Log.e("rentData","rentdata"+carRent);

                        view.showToast("Rent successfully!");

                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideProgessDialog();
                        view.showToast("Fail to rent this vehicle. Please try again later");
                        Log.e("rentData","errorMessage:"+e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        view.hideProgessDialog();
                       view.closeActivity();
                    }
                });
    }

    private void createService(){
        apiService = ApiVehicle.getInstance().create(VehicleService.class);
    }
    private void getCarDetailData(String carId){
        apiService.getCarDetail(carId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CarDetail>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CarDetail carDetail) {
                        Log.e("onNext","cardetail"+carDetail);
                        setData(carDetail);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideProgessDialog();
                        view.showToast("Data is not available");
                        view.disableBtn();
                        Log.e("onError",e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        view.hideProgessDialog();
                    }
                });
    }

    private void setData(CarDetail data){
        view.setCarId(Integer.toString(data.getCarId()));
        view.setName(data.getTitle().equals("") ? "Car "+data.getCarId() : data.getTitle());
        view.setDamagedStatus(data.isIsDamaged() ? "Damaged" : "Good");
        view.setLicensesPlate(data.getLicencePlate());
        view.setFuelLevel(Integer.toString(data.getFuelLevel()));
        view.setStateId(Integer.toString(data.getVehicleStateId()));
        view.setPricingTime(data.getPricingTime().equals("") ? "-" : data.getPricingTime());
        view.setPricingParking(data.getPricingParking().equals("") ? "-" : data.getPricingParking());
        view.setActivatedHardware(data.isIsActivatedByHardware() ? "Yes" : "No");
        view.setLocationId(Integer.toString(data.getLocationId()));
        view.setAddress(data.getAddress().equals("") ? "-" : data.getAddress());
        view.setZipCode(data.getZipCode().equals("") ? "-" : data.getZipCode());
        view.setCity(data.getCity().equals("") ? "-" : data.getCity());
        view.setReservationState(Integer.toString(data.getReservationState()));
        view.setDamageDescription(data.getDamageDescription().equals("") ? "-" : data.getDamageDescription());
        view.setCleanlinessStatus(data.isIsClean() ? "Clean" : "Dirty");
        view.setVehicleTypeId(Integer.toString(data.getVehicleTypeId()));
        view.setCarImage(data.getVehicleTypeImageUrl());
    }

    @Override
    public void onDestroy() {
        view = null;
    }
}
