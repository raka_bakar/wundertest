package com.example.wundertest.ui.cardetail;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.wundertest.R;
import com.example.wundertest.presenter.CarDetailPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CarDetailActivity extends AppCompatActivity implements CarDetailContract.view {
    @BindView(R.id.btn_close_cardetail)
    RelativeLayout btnCloseCardetail;
    @BindView(R.id.iv_car_detail)
    ImageView ivCarDetail;
    @BindView(R.id.tv_carname)
    TextView tvCarname;
    @BindView(R.id.tv_license)
    TextView tvLicense;
    @BindView(R.id.tv_cleanliness)
    TextView tvCleanliness;
    @BindView(R.id.tv_condition)
    TextView tvCondition;
    @BindView(R.id.tv_damage_description)
    TextView tvDamageDescription;
    @BindView(R.id.tv_state_id)
    TextView tvStateId;
    @BindView(R.id.tv_hardware_id)
    TextView tvHardwareId;
    @BindView(R.id.tv_type_id)
    TextView tvTypeId;
    @BindView(R.id.tv_pricing_time)
    TextView tvPricingTime;
    @BindView(R.id.tv_hardware_activated)
    TextView tvHardwareActivated;
    @BindView(R.id.tv_pricing_parking)
    TextView tvPricingParking;
    @BindView(R.id.tv_location_id)
    TextView tvLocationId;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_zip_code)
    TextView tvZipCode;
    @BindView(R.id.tv_city)
    TextView tvCity;
    @BindView(R.id.tv_reservation_state)
    TextView tvReservationState;
    @BindView(R.id.tv_fuel_level)
    TextView tvFuelLevel;
    @BindView(R.id.btn_rent)
    Button btnRent;
    @BindView(R.id.tv_car_id)
    TextView tvCarId;
    @BindView(R.id.pb_cardetail)
    ProgressBar progressBar;

    private CarDetailPresenterImpl presenter;

    private String carId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_detail);
        ButterKnife.bind(this);
        init();
        presenter.loadCarDetail(carId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter = null;
    }

    private void init() {
        presenter = new CarDetailPresenterImpl(this);
        carId = getIntent().getExtras().getString("CAR_ID");
        ButterKnife.bind(this);
    }


    @Override
    public void showProgressDialog() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgessDialog() {

        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setCarId(String carId) {
        tvCarId.setText(carId);
    }

    @Override
    public void setName(String title) {
        tvCarname.setText(title);
    }

    @Override
    public void setCleanlinessStatus(String isClean) {
        tvCleanliness.setText(isClean);
    }

    @Override
    public void setDamagedStatus(String isDamage) {
        tvCondition.setText(isDamage);
    }

    @Override
    public void setLicensesPlate(String licensesPlate) {
        tvLicense.setText(licensesPlate);
    }

    @Override
    public void setFuelLevel(String fuelLevel) {
        tvFuelLevel.setText(fuelLevel);
    }

    @Override
    public void setVehicleTypeId(String vehicleTypeId) {
        tvTypeId.setText(vehicleTypeId);
    }

    @Override
    public void setPricingTime(String pricingTime) {
        tvPricingTime.setText(pricingTime);
    }

    @Override
    public void setPricingParking(String pricingParking) {
        tvPricingParking.setText(pricingParking);
    }

    @Override
    public void setActivatedHardware(String isActivatedHardware) {
        tvHardwareActivated.setText(isActivatedHardware);
    }

    @Override
    public void setLocationId(String locationId) {
        tvLocationId.setText(locationId);
    }

    @Override
    public void setAddress(String address) {
        tvAddress.setText(address);
    }

    @Override
    public void setZipCode(String zipCode) {
        tvZipCode.setText(zipCode);
    }

    @Override
    public void setCity(String city) {
        tvCity.setText(city);
    }

    @Override
    public void setReservationState(String reservationState) {
        tvReservationState.setText(reservationState);
    }

    @Override
    public void setStateId(String stateId) {
        tvStateId.setText(stateId);
    }

    @Override
    public void setDamageDescription(String damageDescription) {
        tvDamageDescription.setText(damageDescription);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    @Override
    public void setCarImage(String url) {
        Glide.with(this).load(url).into(ivCarDetail);

    }

    @Override
    public void disableBtn() {
        btnRent.setEnabled(false);
    }

    @Override
    public void closeActivity() {
        presenter.onDestroy();
        finish();
    }


    @OnClick({R.id.btn_close_cardetail, R.id.btn_rent})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_close_cardetail:
                closeActivity();
                break;
            case R.id.btn_rent:
                presenter.rentVehicle(carId);
                break;
        }
    }
}
